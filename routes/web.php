<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
/*Route::get('login', function () {
    if (\Auth::user())
        return redirect('admin')->with('message', 'You are already logged in');
    return view('login');
});*/

//Route::group(['middleware' => 'auth'], function () {
Route::resource('admin', 'DashViewController');
//});

Route::resource('form-submit', 'FormSubmitController');

Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function () {
    Route::post('login', 'AuthController@login');
    Route::get('logout', 'AuthController@logout');
});