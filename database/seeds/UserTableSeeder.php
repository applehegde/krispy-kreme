<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::create([
            'name' => 'KK Admin',
            'email' => 'admin@kk.com',
            'password' => bcrypt('kk@123')
        ]);
    }
}
