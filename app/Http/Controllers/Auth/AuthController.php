<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;


class AuthController extends Controller
{

    public function login(Request $request)
    {
        $credentials = [
            'email' => $request->get('email'),
            'password' => $request->get('password')
        ];

        $user = User::where('email', $request->get('email'))->first();

        if ($user) {
            if (!\Auth::attempt($credentials)) {
                \Alert::error('Invalid Credentials');
            } else {
                \Alert::info('Welcome');
                return redirect('admin')->with('message', 'Welcome');
            }
        } else {
            \Alert::error('Invalid Credentials');
        }
        return back();
    }

    public function logout()
    {
        \Auth::logout();
        \Alert::info('You are now logged out');
        return redirect('/');
    }
}
