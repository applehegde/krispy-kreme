<?php

namespace App\Http\Controllers;

use App\Models\FormSubmission;

class DashViewController extends Controller
{
    public function index()
    {
        if (!\Auth::user())
            return view('login');

        $submissions = FormSubmission::all();
        return view('dashboard', compact('submissions'));
    }
}
