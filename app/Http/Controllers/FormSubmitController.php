<?php

namespace App\Http\Controllers;


use App\Models\FormSubmission;
use Illuminate\Http\Request;

class FormSubmitController extends Controller
{
    public function store(Request $request)
    {

        FormSubmission::create(
            [
                'name' => $request->get('name'),
                'company_name' => $request->get('company_name'),
                'email' => $request->get('email'),
                'phone' => (int)$request->get('phone'),
                'ip' => $request->ip()
            ]
        );
        \Alert::success('Wel shall contact you soon..', 'Thank you!');
        return back();
    }
}
