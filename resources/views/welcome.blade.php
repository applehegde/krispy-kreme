<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{url("campaign/css/bootstrap.min.css")}}">
    <link rel="stylesheet" href="{{url("campaign/css/style.css")}}">
    <link rel="stylesheet" href="{{url("alert/sweetalert.css")}}">
    <link rel="stylesheet" href="{{url("campaign/css/responsive.css")}}">
    <title>Krispy Kreme</title>
</head>
<body>
<div class="crispy-main-container" id="full-screen-imag">
    <section>
        <div class="container-fluid no-padding">
            <div class="kk-menu">
                <div class="kk-menu-image">
                    <img class="img-responsive" src="{{url("campaign/images/top_dots.png")}}" alt="">
                </div>
                <div class="kk-menu-logo">
                    <a href="http://krispykremeindia.in/">
                        <img class="img-responsive" src="{{url("campaign/images/logo.png")}}" alt="">
                    </a>

                </div>
                <div class="kk-menu-social-icons">
                    <div class="kk-menu-facebook">
                        <a href="https://www.facebook.com/krispykremeindia/" target="_blank">
                            <img class="img-responsive" src="{{url("campaign/images/f.png")}}" alt="">
                        </a>

                    </div>
                    <div class="kk-menu-twiter">
                        <a href=" https://twitter.com/krispykremeind" target="_blank">
                            <img class="img-responsive" src="{{url("campaign/images/t.png")}}" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container-fluid no-padding">
            <div class="kk-main-inner-section">
                <div class="kk-main-banner">
                    <img class="img-responsive" src="{{url("campaign/images/bgs.png")}}" class="img-responsive" alt="">
                </div>
                <div class="kk-bg-banner">
                    <img class="img-responsive" src="{{url("campaign/images/inner-banner.png")}}" alt="">
                </div>
                <div class="container">
                    <div class="kk-cricle-sectio">
                        <div class="kk-cricle-img">
                            <img class="img-responsive" src="{{url("campaign/images/round.png")}}" alt="">
                            <div class="kk-cricle-text">
                                <p>Krispy Kreme is about spreading joy, and a little joy at the workplace goes a long
                                    way.We believe that whether its late night meetings or long Saturdays at office, a
                                    colleague's birthday or an old comrade's farewell, the
                                    sweet taste of Krispy Kreme doughnuts makes every day a little sweeter.</p>
                            </div>
                            <div class="kk-cricle-from">
                                <form class="kk-contact-from" action="{{url("form-submit")}}" method="post">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <input type="text" data-validation="required" name="name" class="form-control"
                                               required id="name"
                                               placeholder="Name">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" required data-validation="required"
                                               name="company_name"
                                               id="companyname" placeholder="Company Name">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" required data-validation="email"
                                               id="emailid" name="email"
                                               placeholder="Email ID">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" required data-validation="number"
                                               name="phone"
                                               id="phonenumber" placeholder="Contact Number">
                                    </div>
                                    <button type="submit" class="kk-button-style">REQUEST CALLBACK</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="kk-testimonial-section">
                        <div class="kk-testimonial-mainwrapper">
                            <img class="img-responsive" src="{{url("campaign/images/testimonials.png")}}" alt="">
                            <!-- <p>TESTIMONIALS</p> -->
                            <div class="kk-testimonial-content">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                    Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                                    unknown printer took a galley of type and scrambled it to
                                    make a type specimen book. </p>
                                <div class="kk-testimonial-authoer">
                                    <p>- Alex Gomez</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container-fluid no-padding">
            <div class="kk-footer">
                <div class="kk-footer-text">
                    <p>Thank You For Choosing Krispy Kreme!</p>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="{{url("campaign/js/jquery.min-1.9.1.js")}}"></script>
<script type="text/javascript" src="{{url("campaign/js/bootstrap.min.js")}}"></script>
<script type="text/javascript" src="{{url("campaign/js/jquery.form-validator.min.js")}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $.validate({
            lang: 'en'
        });
    })
</script>
<script type="text/javascript" src="{{url("alert/sweetalert.min.js")}}"></script>

@include('sweet::alert')
</body>

</html>
