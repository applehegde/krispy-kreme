<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Krispy Kreme</title>
    <link rel="stylesheet" href="{{url("dashboard/css/style.css")}}">
</head>

<body>
<div class="login-page">
    <div class="form">
        <form class="login-form" method="post" action="{{url("auth/login")}}">
            {{csrf_field()}}
            <input type="text" name="email" placeholder="username"/>
            <input type="password" name="password" placeholder="password"/>
            <button>login</button>
        </form>
    </div>
</div>
<script src='//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

<script src="{{url('dashboard/js/index.js')}}"></script>

</body>
</html>
