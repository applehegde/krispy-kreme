@extends('layout')
@section('content')
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Campaign
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Campaign</a></li>
                <li class="active">Form Submissions</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Form Submissions</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table class="table table-bordered table-striped datatable">
                                <thead>
                                <tr>
                                    <td>SL No</td>
                                    <td>Name</td>
                                    <td>Email</td>
                                    <td>Company</td>
                                    <td>Phone</td>
                                    <td>IP</td>
                                    <td>Date Time</td>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($submissions as $submission)
                                    <tr>
                                        <td>{{$submission->id}}</td>
                                        <td>{{$submission->name}}</td>
                                        <td>{{$submission->email}}</td>
                                        <td>{{$submission->company_name}}</td>
                                        <td>{{$submission->phone}}</td>
                                        <td>{{$submission->ip}}</td>
                                        <td>{{$submission->created_at->format('d-m-Y H:i a')}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td>SL No</td>
                                    <td>Name</td>
                                    <td>Email</td>
                                    <td>Company</td>
                                    <td>Phone</td>
                                    <td>IP</td>
                                    <td>Date Time</td>
                                </tr>
                                </tfoot>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
            </div>

        </section><!-- /.content -->
    </aside>
@endsection