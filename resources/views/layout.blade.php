<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Krispy Kreem| Dashboard</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- bootstrap 3.0.2 -->
    <link href="{{url("AdminLTE/css/bootstrap.min.css")}}" rel="stylesheet" type="text/css"/>
    <!-- font Awesome -->
    <link href="{{url("AdminLTE/css/font-awesome.min.css")}}" rel="stylesheet" type="text/css"/>
    <!-- Ionicons -->
    <link href="{{url("AdminLTE/css/ionicons.min.css")}}" rel="stylesheet" type="text/css"/>

    <!-- Theme style -->
    <link href="{{url("AdminLTE/css/AdminLTE.css")}}" rel="stylesheet" type="text/css"/>
    <link href="{{url("AdminLTE/css/datatables/dataTables.bootstrap.css")}}" rel="stylesheet" type="text/css"/>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body class="skin-black">
<!-- header logo: style can be found in header.less -->
<header class="header">
    <a href="#" class="logo">
        <!-- Add the class icon to your logo image or logo icon to add the margining -->
        KK ADMIN
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <div class="navbar-right">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-user"></i>
                        <span>Admin <i class="caret"></i></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="user-body">
                            <a href="{{url('auth/logout')}}">
                                Log Out
                            </a>
                        </li>
                        <!-- Menu Footer-->
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
            </div>
        </section>
        <!-- /.sidebar -->
    </aside>
    @yield('content')
</div><!-- ./wrapper -->

<!-- add new calendar event modal -->


<!-- jQuery 2.0.2 -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<!-- jQuery UI 1.10.3 -->
<script src="{{url("AdminLTE/js/jquery-ui-1.10.3.min.js")}}" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="{{url("AdminLTE/js/bootstrap.min.js")}}" type="text/javascript"></script>


<script src="{{url("AdminLTE/js/plugins/datatables/jquery.dataTables.js")}}" type="text/javascript"></script>
<script src="{{url("AdminLTE/js/plugins/datatables/dataTables.bootstrap.js")}}" type="text/javascript"></script>
<!-- Morris.js charts -->
<script type="text/javascript">
    $(function () {
        $(".datatable").dataTable();
    });
</script>

<!-- AdminLTE App -->


<!-- AdminLTE dashboard demo (This is only for demo purposes) -->

</body>
</html>